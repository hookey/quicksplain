# Quicksplain

Quicksplain is a free party trivia game for groups of 2 to 8 people that tests your knowledge on a variety of topics. It is live at https://quicksplain.com.

Frontend technologies: Vue.js, socket.io, TailwindCSS, VueTailwind, ApexCharts, Plausible

Backend technologies: Node.js, Express.js, socket.io, bookshelf.js, PostgreSQL, Redis

## How to play

When it's your turn, you are given a list of topics to choose from and you have 60 seconds to quickly explain the chosen topic to the other players. They then evaluate your quicksplanation based on keywords that you mention. The best quicksplainer wins!

## Components

This project consists of a frontend, admin panel and backend. The backend has 2 parts: a WebSocket server used by the frontend and an HTTP server used by the admin panel. The backend talks to PostgreSQL and Redis servers.

### Backend

Set the PostgreSQL connection string and the ports in `.env`. Make sure that the databases are running. Run `yarn install` or `npm install` before running.

The WebSocket server is written using socket.io and it is used by the frontend app/game. Run it with `node sockets.js`.

The HTTP server is written using Express.js and it is used by the admin panel app. Run it with `yarn start` or `npm start`.

### Database

You need to run a PostgreSQL server and create a `quicksplain` database: `sudo -u postgres createdb quicksplain`. Make sure the connection string in `backend/.env` is correct.

The database schema can be created using the `quicksplain_schema.sql` file in the `database/` directory with this command: `psql -W -d quicksplain < quicksplain_schema.sql`.

The example schema has no categories, topics or keywords. You will need to add them yourself with the admin panel keywords page, CSV import or SQL commands.

The game states are stored on a Redis server. You will need to run a Redis server on `127.0.0.1:6379`. This is the default address after installing Redis.

### Frontend

This is a the game written in Vue that is played by users on https://quicksplain.com.

Set the WebSocket server URL in the `.env` and `.env.production` files. You may also set a Plausible URL if you want to use it.

Running: `yarn install`, `yarn serve`. Building `yarn build`. (or `npm install`, `npm serve`, `npm build`)

### Admin panel

This is a simple interface written in Vue that can be accessed at https://quicksplain.com/admin. Currently, it can be used to get info about currently active rooms; browse categories, topics and keywords; view and accept/reject feedbacks; keywords data CSV import/export; view some statistics and change the password of the admin user.

The default admin username and password are `admin` and `admin`. The password can be changed in the admin panel.

Set the HTTP server URL in the `.env` and `.env.production` files.

Running: `yarn install`, `yarn serve`. Building `yarn build`. (or `npm install`, `npm serve`, `npm build`)

## Docker

Each component (except the redis server) has a respective Dockerfile and there is a docker-compose example that runs:

- postgres on port 5432,
- redis on 6379,
- the backend API on 5000,
- the backend sockets on 3000.
- the frontend on 8080 and
- the admin panel on 8081.

Make sure to edit the `.env.production` files of the frontends and the other environment variables in the docker-compose file.
