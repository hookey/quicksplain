import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import axios from "axios";
import VueAxios from "vue-axios";
import moment from "vue-moment";
import VueTailwind from "vue-tailwind";
import vueTailwindSettings from "../vue-tailwind.config.js";
import VueApexCharts from "vue-apexcharts";

import "./assets/tailwind.css";
import "./assets/tailwind.css";

Vue.config.productionTip = false;

Vue.use(VueAxios, axios);
Vue.use(moment);

Vue.use(VueApexCharts);
Vue.component("apexchart", VueApexCharts);

Vue.use(VueTailwind, vueTailwindSettings);

if (process.env.VUE_APP_SERVER_URL) {
  Vue.axios.defaults.baseURL = process.env.VUE_APP_SERVER_URL;
}

Vue.axios.defaults.withCredentials = true;

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
