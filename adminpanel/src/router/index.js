import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Login from "../views/Login.vue";
import About from "../views/About.vue";
import Feedback from "../views/Feedback.vue";
import Keywords from "../views/Keywords.vue";
import Account from "../views/Account.vue";
import ImportExport from "../views/ImportExport.vue";
import Stats from "../views/Stats.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
  },
  {
    path: "/about",
    name: "About",
    component: About,
  },
  {
    path: "/feedback",
    name: "Feedback",
    component: Feedback,
  },
  {
    path: "/keywords",
    name: "Keywords",
    component: Keywords,
  },
  {
    path: "/account",
    name: "Account",
    component: Account,
  },
  {
    path: "/importexport",
    name: "Import/Export",
    component: ImportExport,
  },
  {
    path: "/stats",
    name: "Stats",
    component: Stats,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  Vue.axios
    .get("/checklogin")
    .then(() => {
      if (to.path === "/login") next("/");
      else next();
    })
    .catch(() => {
      if (to.path === "/login") next();
      else next("/login");
    });
});

export default router;
