module.exports = {
  purge: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}", "./*.config.js"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      minWidth: {
        "96": "24rem",
      },
    },
  },
  variants: {
    extend: {
      rotate: ["group-hover"],
    },
  },
  plugins: [require("@tailwindcss/forms")],
};
