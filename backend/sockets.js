require("dotenv").config();

const io = require("socket.io")({
  path: process.env.SOCKETS_PATH || "/",
  cors: {
    origin: process.env.FRONTEND_URL || "*",
    // methods: ["GET", "POST"],
  },
  pingTimeout: 120000, // disconnect after 2 minutes of no response
  pingInterval: 5000, // ping every 5 seconds
});

const generateID = require("./utils/generate-id");
const { newGameState, gamePhases } = require("./utils/game");
const redisClient = require("./utils/redis-client");

const Category = require("./models/category");
const Topic = require("./models/topic");
const Keyword = require("./models/keyword");
const Turn = require("./models/turn");
const TopicChoice = require("./models/topicchoice");
const Guess = require("./models/guess");
const Feedback = require("./models/feedback");

const port = process.env.SOCKETS_PORT || 3000;

let gameStates = {};
let countdownTimeouts = {};

function saveGameState(roomCode, gameState) {
  redisClient.hset("quicksplain:gamestates", roomCode, JSON.stringify(gameState));
}

function deleteGameState(roomCode) {
  redisClient.hdel("quicksplain:gamestates", roomCode);
}

io.on("connection", (client) => {
  client.on("createRoom", handleCreateRoom);
  client.on("joinRoom", handleJoinRoom);
  client.on("setName", handleSetName);
  client.on("leaveRoom", handleLeaveRoom);
  client.on("disconnect", handleLeaveRoom);
  client.on("chooseTopic", handleChooseTopic);
  client.on("selectKeyword", handleSelectKeyword);
  client.on("endGuessing", handleEndGuessing);
  client.on("voteNextTurn", handleVoteNextTurn);
  client.on("voteNewRound", handleVoteNewRound);
  client.on("voteCorrection", handleVoteCorrection);
  client.on("leaveFeedback", handleLeaveFeedback);
  client.on("leaveGameFeedback", handleLeaveGameFeedback);

  function handleCreateRoom() {
    let roomCode;
    while (roomCode == null || Object.keys(gameStates).includes(roomCode)) {
      roomCode = generateID(4);
    }

    gameStates[roomCode] = newGameState();

    client.emit("roomCode", roomCode);

    saveGameState(roomCode, gameStates[roomCode]);
  }

  function handleJoinRoom(roomCode) {
    roomCode = roomCode.toUpperCase();
    if (!gameStates.hasOwnProperty(roomCode)) {
      client.emit("kick", `Room ${roomCode} does not exist!`);
      return;
    }

    let gameState = gameStates[roomCode];

    // If player with client id already in room, just ignore the join and send them a state
    // This is just in case a player manages to leave the route without leaving the room
    if (gameState.public.players.hasOwnProperty(client.id)) {
      client.emit("newGameState", JSON.stringify(gameState.public));
      return;
    }

    const room = io.sockets.adapter.rooms.get(roomCode);
    const numClients = room ? room.size : 0;

    if (numClients >= 8) {
      client.emit("kick", `Room ${roomCode} is full!`);
      return;
    }

    if (gameState.public.gamePhase !== gamePhases.LOBBY) {
      client.emit("kick", `Game in ${roomCode} is in progress!`);
      return;
    }

    client.roomCode = roomCode;

    gameState.public.players[String(client.id)] = {
      id: client.id,
      name: "",
      joined: false,
      score: 0,
    };
    gameState.public.joinOrder.push(client.id);
    gameState.public.results.push(client.id);
    gameState.nextRoundPlayers.push(client.id);
    gameState.public.nextTurnVotes = [];

    client.join(roomCode);
    io.sockets.in(roomCode).emit("newGameState", JSON.stringify(gameState.public));

    saveGameState(roomCode, gameState);
  }

  function handleSetName(name) {
    if (name.length < 1 || name.length > 20) {
      client.emit("message", "Name has to be between 1 and 20 characters long.");
      return;
    }
    let roomCode = client.roomCode;
    let gameState = gameStates[roomCode];

    if (!gameState) {
      client.emit("kick", "This room doesn't exist anymore.");
      return;
    }

    if (Object.values(gameState.public.players).some((player) => player.name === name)) {
      client.emit("message", "Someone with that name is already in the room.");
      return;
    }

    // Change player name and set as joined
    gameState.public.players[client.id].name = name;
    gameState.public.players[client.id].joined = true;
    gameStates[roomCode] = gameState;

    io.sockets.in(roomCode).emit("newGameState", JSON.stringify(gameState.public));

    saveGameState(roomCode, gameState);
  }

  function handleLeaveRoom() {
    // Ignore if client not in a room
    if (!client.roomCode) {
      return;
    }
    let roomCode = client.roomCode;
    client.leave(roomCode);
    delete client.roomCode;

    let gameState = gameStates[roomCode];

    let turnPlayerLeft = client.id === gameState.public.nextPlayers[0];
    // let playerName = gameState.public.players[client.id].name;

    // Remove player from everywhere
    delete gameState.public.players[client.id];
    gameState.public.joinOrder = gameState.public.joinOrder.filter((id) => id !== client.id);
    gameState.public.results = gameState.public.results.filter((id) => id !== client.id);
    gameState.public.nextPlayers = gameState.public.nextPlayers.filter((id) => id !== client.id);

    // Remove all votes for the next turn
    gameState.public.nextTurnVotes = [];
    // Remove all votes from keywords and toggle keyword guessed if enough votes without leaver
    for (let keywordID of Object.keys(gameState.public.resultKeywords)) {
      let resultKeyword = gameState.public.resultKeywords[keywordID];
      resultKeyword.votes = resultKeyword.votes.filter((id) => id !== client.id);
      if (resultKeyword.votes.length === gameState.public.joinOrder.length) {
        resultKeyword.guessed = !resultKeyword.guessed;
        resultKeyword.votes = [];
        gameState.public.guessedKeywordCount = Object.values(gameState.public.resultKeywords).filter(
          (keyword) => keyword.guessed
        ).length;
      }
    }
    gameState.nextRoundPlayers = gameState.nextRoundPlayers.filter((id) => id !== client.id);

    if (gameState.public.joinOrder.length === 0) {
      delete gameStates[roomCode];
      deleteGameState(roomCode);
      return;
    } else if (gameState.public.joinOrder.length === 1) {
      // If one player remaining, return to lobby
      gameState.nextRoundPlayers.push(gameState.public.joinOrder[0]);
      gameState.public.gamePhase = gamePhases.LOBBY;
      clearTimeout(countdownTimeouts[roomCode]);
      delete countdownTimeouts[roomCode];
      io.sockets.in(roomCode).emit("newGameState", JSON.stringify(gameState.public));
    } else if (
      turnPlayerLeft &&
      (gameState.public.gamePhase === gamePhases.TOPIC_SELECTION || gameState.public.gamePhase === gamePhases.GUESSING)
    ) {
      // Start new turn if the quicksplainer leaves during topic selection or guessing
      // io.sockets.in(roomCode).emit("message", `${playerName} has disconnected. Skipping turn.`);
      nextTurn(roomCode);
    } else {
      if (gameState.public.gamePhase !== gamePhases.LOBBY) {
        // io.sockets.in(roomCode).emit("message", `${playerName} has disconnected.`);
      }
      io.sockets.in(roomCode).emit("newGameState", JSON.stringify(gameState.public));
    }

    saveGameState(roomCode, gameState);
  }

  function handleVoteNewRound() {
    const roomCode = client.roomCode;
    let gameState = gameStates[roomCode];

    if (!gameState) {
      client.emit("kick", "This room doesn't exist anymore.");
      return;
    }

    // Reject if current phase isn't the lobby phase
    if (gameState.public.gamePhase !== gamePhases.LOBBY) {
      return;
    }

    // Reject if any players have not entered a name
    if (Object.values(gameState.public.players).some((player) => !player.joined)) {
      return;
    }

    // Toggle client ID in nextTurnVotes array
    if (gameState.public.nextTurnVotes.includes(client.id)) {
      gameState.public.nextTurnVotes = gameState.public.nextTurnVotes.filter((id) => id !== client.id);
    } else {
      gameState.public.nextTurnVotes.push(client.id);
    }

    if (gameState.public.nextTurnVotes.length === gameState.public.joinOrder.length) {
      gameState.public.nextTurnVotes = [];
      gameState.public.round += 1;

      if (gameState.public.round === 1) {
        const startingPlayerIndex = Math.floor(Math.random() * gameState.nextRoundPlayers.length);
        // Create next players queue with random starting player for the first round
        gameState.public.nextPlayers = [
          ...gameState.nextRoundPlayers.splice(startingPlayerIndex),
          ...gameState.nextRoundPlayers.splice(0, startingPlayerIndex),
        ];
      } else {
        gameState.public.nextPlayers = gameState.nextRoundPlayers.splice(0);
      }

      newTurn(roomCode);
    } else {
      io.in(roomCode).emit("newGameState", JSON.stringify(gameState.public));
      saveGameState(roomCode, gameState);
    }
  }

  function newTurn(roomCode) {
    let gameState = gameStates[roomCode];
    gameState.public.gamePhase = gamePhases.TOPIC_SELECTION;

    const turnPlayerID = gameState.public.nextPlayers[0];

    // Get topics from all enabled categories
    Category.query((qb) => {
      qb.select("id");
      qb.where("enabled", true);
      qb.orderBy("id");
    })
      .fetchAll({
        require: true,
      })
      .then((categories) => {
        // Make separate select calls for all categories to get some random topics
        let promises = [];
        for (let category of categories) {
          promises.push(
            Topic.query((qb) => {
              qb.select(["topic.id", "topic.name", "category.name as category"]);
              qb.join("category", "category.id", "=", "topic.category_id");
              qb.where("topic.enabled", true);
              qb.where("category_id", category.get("id"));
              qb.orderByRaw("random()");
              qb.limit(4);
            }).fetchAll({
              require: true,
            })
          );
        }
        // Wait for all promises to finish
        Promise.all(promises)
          .then((categories) => {
            // `categories` is an array of arrays of topics in a category
            // Flatten the array to get an array of topic objects with id, name and category
            let topics = categories.map((category) => category.toJSON()).flat();

            gameState.turnTopicChoices = topics;
            gameState.public.secondsRemaining = 20;
            countdownTimeouts[roomCode] = setTimeout(countDown, 1000, roomCode, startWithRandomTopic);

            io.in(roomCode).emit("newGameState", JSON.stringify(gameState.public));
            io.to(turnPlayerID).emit("turnTopicChoices", JSON.stringify(topics));
            saveGameState(roomCode, gameState);
          })
          .catch((err) => console.error(err));
      })
      .catch((err) => console.error(err));
  }

  function handleChooseTopic(topicIDString) {
    let topicID = Number(topicIDString);
    if (isNaN(topicID)) {
      return;
    }

    const roomCode = client.roomCode;
    let gameState = gameStates[roomCode];

    if (!gameState) {
      client.emit("kick", "This room doesn't exist anymore.");
      return;
    }

    let turnPlayerID = gameState.public.nextPlayers[0];

    // Reject if current phase isn't the topic selection phase
    if (gameState.public.gamePhase !== gamePhases.TOPIC_SELECTION) {
      return;
    }

    // Reject choice if client isn't turn player
    if (client.id !== turnPlayerID) {
      return;
    }

    // Reject choice if topic ID not in round's topic selection
    if (!gameState.turnTopicChoices.some((topic) => topic.id == topicID)) {
      console.log("Received topic choice not in selection.");
      return;
    }

    startGuessing(roomCode, topicID, false);
  }

  function startWithRandomTopic(roomCode) {
    let gameState = gameStates[roomCode];

    let randomIndex = Math.floor(Math.random() * gameState.turnTopicChoices.length);
    let randomTopicID = gameState.turnTopicChoices[randomIndex].id;

    startGuessing(roomCode, randomTopicID, true);
  }

  function startGuessing(roomCode, chosenTopicID, isRandomTopic) {
    let gameState = gameStates[roomCode];
    let turnPlayerID = gameState.public.nextPlayers[0];

    new Topic({ id: chosenTopicID })
      .fetch({
        withRelated: ["randomEnabledKeywords"],
        require: true,
        columns: ["id", "name"],
      })
      .then((topic) => {
        topic = topic.toJSON();
        let keywords = topic.randomEnabledKeywords.slice();
        let playerCount = gameState.public.joinOrder.length;
        let keywordsPerPlayer = Math.ceil(keywords.length / (playerCount - 1));

        gameState.public.turnTopic = topic;
        gameState.public.turnKeywordCount = keywords.length;
        gameState.public.guessedKeywordCount = 0;
        gameState.public.gamePhase = gamePhases.GUESSING;
        gameState.playerKeywords = {};
        gameState.public.secondsRemaining = 60;
        clearTimeout(countdownTimeouts[roomCode]);
        countdownTimeouts[roomCode] = setTimeout(countDown, 1000, roomCode, showResults);
        io.in(roomCode).emit("newGameState", JSON.stringify(gameState.public));

        gameState.public.joinOrder
          .filter((playerID) => playerID !== turnPlayerID)
          .forEach((playerID) => {
            let playerKeywords = keywords.splice(0, keywordsPerPlayer);
            gameState.playerKeywords[playerID] = playerKeywords.map((keyword) => {
              return { ...keyword, guessed: false, votes: [] };
            });
            io.to(playerID).emit("turnKeywords", JSON.stringify(playerKeywords));
          });

        saveGameState(roomCode, gameState);
      })
      .catch((err) => {
        console.error(err);
      });

    // Save turn and topic choices to database
    new Turn({
      topic_id: chosenTopicID,
      timestamp: new Date(),
      random_topic: isRandomTopic,
    })
      .save()
      .then((turn) => {
        gameState.turnID = turn.get("id");

        for (let topic of gameState.turnTopicChoices) {
          let chosen = false;
          if (topic.id === chosenTopicID) {
            chosen = true;
          }
          new TopicChoice({
            turn_id: turn.get("id"),
            topic_id: topic.id,
            chosen: chosen,
          })
            .save()
            .catch((err) => {
              console.error(err);
            });
        }
      })
      .catch((err) => {
        console.error(err);
      });
  }

  function countDown(roomCode, endFunction) {
    let gameState = gameStates[roomCode];

    gameState.public.secondsRemaining -= 1;
    io.in(roomCode).emit("secondsRemaining", gameState.public.secondsRemaining);
    saveGameState(roomCode, gameState);

    if (gameState.public.secondsRemaining <= 0) {
      endFunction(roomCode);
    } else {
      countdownTimeouts[roomCode] = setTimeout(countDown, 1000, roomCode, endFunction);
    }
  }

  function handleSelectKeyword(keywordIDString) {
    keywordID = Number(keywordIDString);
    if (isNaN(keywordID)) {
      return;
    }

    const roomCode = client.roomCode;
    let gameState = gameStates[roomCode];

    if (!gameState) {
      client.emit("kick", "This room doesn't exist anymore.");
      return;
    }

    let turnPlayerID = gameState.public.nextPlayers[0];

    // Reject if current phase isn't the guessing phase
    if (gameState.public.gamePhase !== gamePhases.GUESSING) {
      return;
    }

    // Reject selection if client is turn player
    if (client.id === turnPlayerID) {
      return;
    }

    // Reject selection if keyword ID not in player's keyword list
    if (!gameState.playerKeywords[client.id].some((keyword) => keyword.id === keywordID)) {
      console.log("Received keyword choice not in selection. 🤔");
      return;
    }

    // Toggle guessed state of selected keyword
    gameState.playerKeywords[client.id] = gameState.playerKeywords[client.id].map((keyword) =>
      keyword.id === keywordID ? { ...keyword, guessed: !keyword.guessed } : keyword
    );

    let guessedKeywordCount = 0;
    for (let playerKeywords of Object.values(gameState.playerKeywords)) {
      guessedKeywordCount += playerKeywords.filter((keyword) => keyword.guessed).length;
    }
    gameState.public.guessedKeywordCount = guessedKeywordCount;

    io.in(roomCode).emit("newGameState", JSON.stringify(gameState.public));
    saveGameState(roomCode, gameState);
  }

  function handleEndGuessing() {
    const roomCode = client.roomCode;
    let gameState = gameStates[roomCode];
    if (!gameState) {
      client.emit("kick", "This room doesn't exist anymore.");
      return;
    }

    let turnPlayerID = gameState.public.nextPlayers[0];

    // Reject choice if client isn't turn player
    if (client.id !== turnPlayerID) {
      return;
    }

    clearTimeout(countdownTimeouts[roomCode]);
    delete countdownTimeouts[roomCode];

    showResults(roomCode);
  }

  function showResults(roomCode) {
    let gameState = gameStates[roomCode];

    gameState.public.gamePhase = gamePhases.RESULTS;

    let resultKeywords = {};
    let resultKeywordOrder = [];
    for (let playerKeywords of Object.values(gameState.playerKeywords)) {
      for (let playerKeyword of playerKeywords) {
        resultKeywords[playerKeyword.id] = {
          name: playerKeyword.name,
          guessed: playerKeyword.guessed,
          votes: [],
        };
        resultKeywordOrder.push(playerKeyword.id);
      }
    }
    gameState.public.resultKeywords = resultKeywords;
    gameState.public.resultKeywordOrder = resultKeywordOrder;

    // sort server-side, so order doesn't change after voting changes
    gameState.public.resultKeywordOrder.sort(
      (a, b) => resultKeywords[a].name.localeCompare(resultKeywords[b].name) // alphabetically
    );
    gameState.public.resultKeywordOrder.sort(
      (a, b) => resultKeywords[b].guessed - resultKeywords[a].guessed // guessed keywords first
    );
    io.in(roomCode).emit("newGameState", JSON.stringify(gameState.public));
    saveGameState(roomCode, gameState);
  }

  function handleVoteCorrection(keywordIDString) {
    keywordID = Number(keywordIDString);
    if (isNaN(keywordID)) {
      return;
    }

    const roomCode = client.roomCode;
    let gameState = gameStates[roomCode];

    if (!gameState) {
      client.emit("kick", "This room doesn't exist anymore.");
      return;
    }

    let keyword = gameState.public.resultKeywords[keywordIDString];

    // Reject if keyword doesn't exist
    if (!keyword) {
      console.log("Someone tried voting on a keyword that isn't in the result keywords. 🤔");
      return;
    }

    // Toggle client ID in votes array of selected keyword
    if (keyword.votes.includes(client.id)) {
      keyword.votes = keyword.votes.filter((id) => id !== client.id);
    } else {
      keyword.votes.push(client.id);
    }

    // Toggle keyword guessed status
    if (keyword.votes.length === gameState.public.joinOrder.length) {
      keyword.votes = [];
      keyword.guessed = !keyword.guessed;
      gameState.public.guessedKeywordCount = Object.values(gameState.public.resultKeywords).filter(
        (keyword) => keyword.guessed
      ).length;
    }

    io.in(roomCode).emit("newGameState", JSON.stringify(gameState.public));
    saveGameState(roomCode, gameState);
  }

  function handleVoteNextTurn() {
    const roomCode = client.roomCode;
    let gameState = gameStates[roomCode];

    if (!gameState) {
      client.emit("kick", "This room doesn't exist anymore.");
      return;
    }

    // Reject if current phase isn't turn results
    if (gameState.public.gamePhase !== gamePhases.RESULTS) {
      return;
    }

    // Toggle client ID in nextTurnVotes array
    if (gameState.public.nextTurnVotes.includes(client.id)) {
      gameState.public.nextTurnVotes = gameState.public.nextTurnVotes.filter((id) => id !== client.id);
    } else {
      gameState.public.nextTurnVotes.push(client.id);
    }

    if (gameState.public.nextTurnVotes.length === gameState.public.joinOrder.length) {
      gameState.public.nextTurnVotes = [];
      nextTurn(roomCode);
    } else {
      io.in(roomCode).emit("newGameState", JSON.stringify(gameState.public));
      saveGameState(roomCode, gameState);
    }
  }

  function nextTurn(roomCode) {
    let gameState = gameStates[roomCode];

    // Update score of current player
    let turnPlayerID = gameState.public.nextPlayers[0];
    let score = gameState.public.guessedKeywordCount * 100;
    gameState.public.players[turnPlayerID].score += score;

    // Move current player to the queue of players to play in the next round
    gameState.nextRoundPlayers.push(...gameState.public.nextPlayers.splice(0, 1));

    if (gameState.public.nextPlayers.length === 0) {
      showRoundResults(roomCode);
    } else {
      newTurn(roomCode);
    }

    // Save guesses to database
    for (let [keywordID, keyword] of Object.entries(gameState.public.resultKeywords)) {
      new Guess({
        turn_id: gameState.turnID,
        keyword_id: keywordID,
        guessed: keyword.guessed,
      })
        .save()
        .then(() => {
          delete gameState.public.resultKeywords[keywordID];
        })
        .catch((err) => {
          console.error(err);
        });
    }
  }

  function showRoundResults(roomCode) {
    let gameState = gameStates[roomCode];

    gameState.public.gamePhase = gamePhases.LOBBY;
    gameState.public.results = [...gameState.public.joinOrder].sort(
      (a, b) => gameState.public.players[b].score - gameState.public.players[a].score
    );

    io.in(roomCode).emit("newGameState", JSON.stringify(gameState.public));
    saveGameState(roomCode, gameState);
  }

  function handleLeaveFeedback(feedbackTypeString, keywordIDString, comment, newKeyword) {
    let keywordID = Number(keywordIDString);
    let feedbackType = Number(feedbackTypeString);
    if (![1, 2, 3, 4].includes(feedbackType) || comment.length > 128 || newKeyword.length > 128) {
      return;
    }
    if (isNaN(keywordID) || feedbackType === 1 || feedbackType === 2) {
      keywordID = null;
    }
    if (feedbackType === 1 || feedbackType === 3) {
      newKeyword == "";
    }
    if (!keywordID && !comment && !newKeyword) {
      return;
    }

    const roomCode = client.roomCode;
    let gameState = gameStates[roomCode];

    if (!gameState) {
      // TODO: Still save feedback in case server restarted mid-game?
      client.emit("kick", "This room doesn't exist anymore.");
      return;
    }

    // Prevent changing a keyword's name to the old name
    let oldname = keywordID ? gameState.public.resultKeywords[keywordID].name : "";
    if (feedbackType === 4 && oldname === newKeyword) {
      return;
    }

    feedbackObject = {
      feedbacktype_id: feedbackType,
      feedbackstatus_id: 1, // new
      turn_id: gameState.turnID,
      topic_id: gameState.public.turnTopic.id,
      keyword_id: keywordID,
      comment: comment,
      newname: newKeyword,
      oldname: oldname,
      timestamp: new Date(),
    };

    new Feedback(feedbackObject)
      .save()
      .then(() => {
        client.emit("feedbackSuccess");
      })
      .catch((err) => {
        client.emit("message", "There was an error with the feedback.");
        console.error(err);
      });
  }

  function handleLeaveGameFeedback(feedbackText) {
    if (feedbackText.length == 0 || feedbackText.length > 320) {
      return;
    }

    // Rate limit client to 1 feedback per 5 seconds
    if (client.lastFeedback && new Date() - client.lastFeedback < 5000) {
      client.emit("message", "You are doing that too often. Try again in a few seconds.");
      return;
    }

    feedbackObject = {
      feedbacktype_id: 1, // comment
      feedbackstatus_id: 1, // new
      comment: feedbackText,
      timestamp: new Date(),
    };

    new Feedback(feedbackObject)
      .save()
      .then(() => {
        client.lastFeedback = new Date();
        client.emit("gameFeedbackSuccess");
      })
      .catch((err) => {
        client.emit("message", "There was an error with the feedback.");
        console.error(err);
      });
  }
});

io.listen(port);
