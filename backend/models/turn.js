const bookshelf = require("../utils/bookshelf");

const Turn = bookshelf.model("Turn", {
  tableName: "turn",
  topic() {
    return this.belongsTo("Topic");
  },
  topicChoice() {
    return this.hasMany("TopicChoice");
  },
  guesses() {
    return this.hasMany("Guess");
  },
  feedbacks() {
    return this.hasMany("Feedback");
  },
});

module.exports = Turn;
