const bookshelf = require("../utils/bookshelf");

const TopicChoice = bookshelf.model("TopicChoice", {
  tableName: "topicchoice",
  turn() {
    return this.belongsTo("Turn");
  },
  topic() {
    return this.belongsTo("Topic");
  },
});

module.exports = TopicChoice;
