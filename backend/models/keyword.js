const bookshelf = require("../utils/bookshelf");

const Keyword = bookshelf.model("Keyword", {
  tableName: "keyword",
  hidden: [],
  topic() {
    return this.belongsTo("Topic");
  },
});

module.exports = Keyword;
