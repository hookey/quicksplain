const bookshelf = require("../utils/bookshelf");

const Category = bookshelf.model("Category", {
  tableName: "category",
  hidden: [],
  topics() {
    return this.hasMany("Topic").query((qb) => {
      qb.select("*");
      qb.orderBy("name");
    });
  },
  randomEnabledTopics() {
    // deprecated
    return this.hasMany("Topic").query((qb) => {
      qb.select("*");
      qb.where("enabled", true);
      qb.orderByRaw("random()");
      qb.limit(4);
    });
  },
});

module.exports = Category;
