const bookshelf = require("../utils/bookshelf");

const FeedbackType = bookshelf.model("FeedbackType", {
  tableName: "feedbacktype",
  feedbacks() {
    return this.hasMany("Feedback");
  },
});

module.exports = FeedbackType;
