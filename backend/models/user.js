const bookshelf = require("../utils/bookshelf");

const User = bookshelf.model("User", {
  tableName: "user",
  role() {
    return this.belongsTo("Role");
  },
});

module.exports = User;
