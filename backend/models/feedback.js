const bookshelf = require("../utils/bookshelf");

const Feedback = bookshelf.model("Feedback", {
  tableName: "feedback",
  turn() {
    return this.belongsTo("Turn");
  },
  topic() {
    return this.belongsTo("Topic");
  },
  keyword() {
    return this.belongsTo("Keyword");
  },
  feedbacktype() {
    return this.belongsTo("FeedbackType");
  },
  feedbackstatus() {
    return this.belongsTo("FeedbackStatus");
  },
  author() {
    return this.belongsTo("User", "author_id");
  },
  reviewer() {
    return this.belongsTo("User", "reviewer_id");
  },
});

module.exports = Feedback;
