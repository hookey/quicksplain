const createError = require("http-errors");

const requiresLogin = (req, res, next) => {
  if (req.session && req.session.userID) {
    return next();
  }
  next(createError(403, "Login required"));
};

module.exports = requiresLogin;
