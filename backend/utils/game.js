function newGameState() {
  let gameState = {
    public: {
      round: 0,
      gamePhase: gamePhases.LOBBY,
      players: {}, // player id (str) -> {name (str), joined (bool), score (int)}, TODO: should probably be a class
      joinOrder: [], // player ids by joining order
      results: [], // player ids sorted by score
      nextPlayers: [], // player ids yet to play in this round
      turnTopic: null,
      turnKeywordCount: 0,
      guessedKeywordCount: 0,
      turnKeywords: [], // array of {id (int), keyword (str)}
      resultKeywords: {}, // id (int) -> {name (str), guessed (bool), votes (array of player ids) }, TODO: should probably make a class for them
      resultKeywordOrder: [], // array of keyword ids
      nextTurnVotes: [], // array of player ids
      secondsRemaining: 60,
    },
    countdownTimeout: null,
    nextRoundPlayers: [],
    turnTopicChoices: [],
    playerKeywords: {}, // player id (str) ->
    turnID: null,
  };

  return gameState;
}

const gamePhases = Object.freeze({
  LOBBY: 0,
  TOPIC_SELECTION: 1,
  GUESSING: 2,
  RESULTS: 3,
  ROUND_RESULTS: 4,
});

module.exports = {
  newGameState,
  gamePhases,
};
