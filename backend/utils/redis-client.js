const redis = require("redis");
const redis_url = process.env.REDIS_URL || "redis://localhost:6379";
const client = redis.createClient(redis_url);

module.exports = client;
