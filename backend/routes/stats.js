const express = require("express");
const router = express.Router();

const Category = require("../models/category");
const Topic = require("../models/topic");
const Keyword = require("../models/keyword");
const User = require("../models/user");
const Feedback = require("../models/feedback");
const FeedbackType = require("../models/feedbacktype");
const FeedbackStatus = require("../models/feedbackstatus");
const Turn = require("../models/turn");
const bookshelf = require("../utils/bookshelf");
const requiresLogin = require("../utils/admin-utils");
const { query } = require("express");
const { Collection } = require("bookshelf");

router.get("/getdatacounts", requiresLogin, async (req, res, next) => {
  let data = {
    categories: await Category.count(),
    topics: await Topic.count(),
    keywords: await Keyword.count(),
    feedbacks: await Feedback.count(),
    turns: await Turn.count(),
  };
  res.json({ success: true, data: data });
});

router.get("/getturnhistory", requiresLogin, async (req, res, next) => {
  bookshelf.knex
    .raw("SELECT to_char(timestamp, 'YYYY-MM-DD') as day, count(*) FROM turn GROUP BY 1 ORDER BY 1;")
    .then((turnsPerDay) => {
      res.json({ success: true, data: turnsPerDay.rows });
    })
    .catch((err) => next(err));
});

module.exports = router;
