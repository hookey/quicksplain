const express = require("express");
const router = express.Router();
const createError = require("http-errors");
const bcrypt = require("bcrypt");
const rateLimit = require("express-rate-limit");

const Category = require("../models/category");
const Topic = require("../models/topic");
const Keyword = require("../models/keyword");
const User = require("../models/user");
const Feedback = require("../models/feedback");
const FeedbackType = require("../models/feedbacktype");
const FeedbackStatus = require("../models/feedbackstatus");
const Turn = require("../models/turn");
const bookshelf = require("../utils/bookshelf");
const requiresLogin = require("../utils/admin-utils");
const { query } = require("express");

const loginLimiter = rateLimit({
  windowMs: 60 * 60 * 1000, // 1 hour window
  max: 10, // start blocking after 20 requests
  message: {
    success: false,
    message: "Too many logins from this IP, please try again after an hour.",
  },
});

router.get("/", requiresLogin, (req, res, next) => {
  res.json({ hello: "there" });
});

// router.get("/login", (req, res, next) => {
//   req.session.userID = 1;
//   res.json({ success: true, message: "Logged in" });
// });

router.post("/login", loginLimiter, (req, res, next) => {
  if (!req.body.username || !req.body.password) {
    next(createError(401, "Need username and password"));
  }

  new User({ username: req.body.username })
    .fetch()
    .then(async (user) => {
      user = user.toJSON();
      const isCorrectPassword = await bcrypt.compare(req.body.password, user.password_hash);

      if (isCorrectPassword) {
        req.session.userID = user.id;
        return res.json({ success: true, message: "Logged in" });
      } else {
        next(createError(401, "Incorrect username or password."));
      }
    })
    .catch((err) => {
      next(createError(401, "Incorrect username or password."));
    });
});

router.get("/logout", (req, res, next) => {
  if (req.session) {
    req.session.destroy();
  }
  return res.json({ success: true, message: "Logged out" });
});

router.get("/checklogin", (req, res) => res.status(req.session.userID ? 200 : 401).send("OK"));

router.post("/changepassword", (req, res, next) => {
  if (req.body.newpassword1 === req.body.newpassword2) {
    if (req.body.newpassword1.length >= 8) {
      new User({ id: req.session.userID })
        .fetch()
        .then(async (user) => {
          const isCorrectPassword = await bcrypt.compare(req.body.oldpassword, user.get("password_hash"));
          if (isCorrectPassword) {
            const newPasswordHash = await bcrypt.hash(req.body.newpassword1, 12);
            user
              .save({ password_hash: newPasswordHash })
              .then(() => {
                res.json({ success: true, message: "Password changed!" });
              })
              .catch((err) => {
                next(err);
              });
          } else {
            throw new Error("Incorrect old password.");
          }
        })
        .catch((err) => {
          next(err);
        });
    } else {
      throw new Error("New password must be at least 8 characters.");
    }
  } else {
    throw new Error("Passwords don't match.");
  }
});

router.get("/turncategories", requiresLogin, (req, res, next) => {
  Category.query((qb) => {
    qb.select("id");
    qb.where("enabled", true);
    qb.orderBy("id");
  })
    .fetchAll({
      require: true,
      debug: true,
    })
    .then((categories) => {
      let promises = [];
      for (let category of categories) {
        promises.push(
          Topic.query((qb) => {
            qb.select(["topic.id", "topic.name", "category.name as category"]);
            qb.join("category", "category.id", "=", "topic.category_id");
            qb.where("topic.enabled", true);
            qb.where("category_id", category.get("id"));
            qb.orderByRaw("random()");
            qb.limit(4);
          }).fetchAll({
            require: true,
            debug: true,
          })
        );
      }
      Promise.all(promises)
        .then((results) => {
          let topics = results.map((category) => category.toJSON()).flat();
          res.json({ success: true, results: topics });
        })
        .catch((err) => next(err));
    })
    .catch((err) => {
      next(err);
    });
});

module.exports = router;
