const express = require("express");
const router = express.Router();
const createError = require("http-errors");

const Category = require("../models/category");
const Topic = require("../models/topic");
const Keyword = require("../models/keyword");
const User = require("../models/user");
const Feedback = require("../models/feedback");
const FeedbackType = require("../models/feedbacktype");
const FeedbackStatus = require("../models/feedbackstatus");
const Turn = require("../models/turn");
const bookshelf = require("../utils/bookshelf");
const requiresLogin = require("../utils/admin-utils");
const { query } = require("express");
const json2csv = require("json2csv");
const formidable = require("formidable");
const csv = require("fast-csv");
const fs = require("fs");
const { resolveSoa } = require("dns");

router.get("/export", requiresLogin, (req, res, next) => {
  let includeDisabled = req.query.includeDisabled !== "false"; // default true

  Keyword.query((qb) => {
    qb.select([
      "category.name as category",
      "category.enabled as category_enabled",
      "topic.name as topic",
      "topic.enabled as topic_enabled",
      "keyword.name as keyword",
      "keyword.enabled as keyword_enabled",
    ]);
    if (!includeDisabled) {
      console.log(" where stuff");
      qb.where("category.enabled", true);
      qb.andWhere("topic.enabled", true);
      qb.andWhere("keyword.enabled", true);
    }
    qb.join("topic", "topic.id", "=", "keyword.topic_id");
    qb.join("category", "category.id", "=", "topic.category_id");
    qb.orderBy(["category.id", "topic.name", "keyword.name"]);
  })
    .fetchAll({
      require: false,
    })
    .then((keywords) => {
      const keywordsCSV = json2csv.parse(keywords.toJSON());
      res.attachment("quicksplain_data.csv");
      res.status(200).send(keywordsCSV);
    })
    .catch((err) => next(err));
});

router.post("/import", requiresLogin, (req, res, next) => {
  const form = formidable();

  form.parse(req, (err, fields, files) => {
    if (err) {
      next(err);
      return;
    }
    let rows = [];
    fs.createReadStream(files.csvFile.path)
      .pipe(csv.parse({ headers: true }))
      .on("error", (err) => {
        console.error(err);
        console.log("Setting error status");
        res.status(500).json({ success: false, error: err.message });
      })
      .on("data", (row) => rows.push(row))
      .on("end", async (rowCount) => {
        let newCategories = 0;
        let newTopics = 0;
        let newKeywords = 0;

        try {
          for (let row of rows) {
            // Find category
            let category = await new Category({ name: row.category }).fetch({ require: false });
            if (!category) {
              // Create category if it doesn't exist
              newCategories++;
              category = await new Category({
                name: row.category,
                enabled: row.category_enabled,
              }).save();
            } else if (category.get("enabled").toString() !== row.category_enabled) {
              // Update enabled field if category exists and values are different
              category = await category.save({ enabled: row.category_enabled });
            }

            let topic = await new Topic({ name: row.topic, category_id: category.get("id") }).fetch({
              require: false,
            });

            if (!topic) {
              // Create topic if it doesn't exist
              newTopics++;
              topic = await new Topic({
                name: row.topic,
                enabled: row.topic_enabled,
                category_id: category.get("id"),
              }).save();
            } else if (topic.get("enabled").toString() !== row.topic_enabled) {
              // Update enabled field if topic exists and values are different
              topic = await topic.save({ enabled: row.topic_enabled });
            }

            let keyword = await new Keyword({ name: row.keyword, topic_id: topic.get("id") }).fetch({
              require: false,
            });

            if (!keyword) {
              // Create keyword if it doesn't exist
              newKeywords++;
              keyword = await new Keyword({
                name: row.keyword,
                enabled: row.keyword_enabled,
                topic_id: topic.get("id"),
              }).save();
            } else if (keyword.get("enabled").toString() !== row.keyword_enabled) {
              // Update enabled field if keyword exists and values are different
              keyword = await keyword.save({ enabled: row.keyword_enabled });
            }

            await keyword.save();
          }
          res.json({
            success: true,
            data: {
              rowCount: rowCount,
              newCategories: newCategories,
              newTopics: newTopics,
              newKeywords: newKeywords,
            },
          });
        } catch (err) {
          next(err);
        }
      });
  });
});

module.exports = router;
