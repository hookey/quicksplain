const express = require("express");
const router = express.Router();
const createError = require("http-errors");

const Category = require("../models/category");
const Topic = require("../models/topic");
const Keyword = require("../models/keyword");
const User = require("../models/user");
const Feedback = require("../models/feedback");
const FeedbackType = require("../models/feedbacktype");
const FeedbackStatus = require("../models/feedbackstatus");
const Turn = require("../models/turn");
const bookshelf = require("../utils/bookshelf");
const requiresLogin = require("../utils/admin-utils");
const { query } = require("express");

router.get("/getfeedbacks", requiresLogin, (req, res, next) => {
  Feedback.collection()
    .fetch({
      withRelated: [
        "feedbackstatus",
        "feedbacktype",
        "turn",
        "topic",
        "topic.keywords",
        "keyword",
        "author",
        "reviewer",
      ],
    })
    .then((feedbacks) => {
      res.json(feedbacks);
    })
    .catch((err) => next(err));
});

router.put("/approvefeedback", requiresLogin, (req, res, next) => {
  new Feedback({ id: req.body.id })
    .fetch()
    .then((feedback) => {
      if (feedback.get("feedbacktype_id") === 1) {
        // Comment feedback approval only changes the feedback status to approved
        feedback
          .save({ feedbackstatus_id: 3, reviewer_id: req.session.userID }, { patch: true })
          .then(() => {
            res.json({ success: true });
          })
          .catch((err) => next(err));
      } else if (feedback.get("feedbacktype_id") === 2) {
        // On keyword addition feedback, find keyword and turn enabled true if it already exists
        // Otherwise create new keyword
        new Keyword({
          name: feedback.get("newname"),
          topic_id: feedback.get("topic_id"),
        })
          .fetch({ require: false })
          .then((keyword) => {
            // require: false => fetched model is null if it doesn't exist
            if (keyword) {
              keyword.save({ enabled: true }, { patch: true }).then(() => {
                feedback
                  .save(
                    { feedbackstatus_id: 3, reviewer_id: req.session.userID, keyword_id: keyword.get("id") },
                    { patch: true }
                  )
                  .then(() => {
                    res.json({ success: true });
                  })
                  .catch((err) => next(err));
              });
            } else {
              new Keyword({
                name: feedback.get("newname"),
                topic_id: feedback.get("topic_id"),
                enabled: true,
              })
                .save()
                .then((keyword) => {
                  feedback
                    .save(
                      { feedbackstatus_id: 3, reviewer_id: req.session.userID, keyword_id: keyword.get("id") },
                      { patch: true }
                    )
                    .then(() => {
                      res.json({ success: true });
                    })
                    .catch((err) => next(err));
                })
                .catch((err) => next(err));
            }
          });
      } else if (feedback.get("feedbacktype_id") === 3) {
        // On keyword deletion feedback, set the enabled field to false
        new Keyword({ id: feedback.get("keyword_id") }).fetch().then((keyword) => {
          keyword
            .save({ enabled: false }, { patch: true })
            .then(() => {
              feedback
                .save({ feedbackstatus_id: 3, reviewer_id: req.session.userID }, { patch: true })
                .then(() => {
                  res.json({ success: true });
                })
                .catch((err) => next(err));
            })
            .catch((err) => next(err));
        });
      } else if (feedback.get("feedbacktype_id") === 4) {
        // On keyword modification, set the name field to the new name
        new Keyword({ id: feedback.get("keyword_id") }).fetch().then((keyword) => {
          keyword
            .save({ name: feedback.get("newname") }, { patch: true })
            .then(() => {
              feedback
                .save({ feedbackstatus_id: 3, reviewer_id: req.session.userID }, { patch: true })
                .then(() => {
                  res.json({ success: true });
                })
                .catch((err) => next(err));
            })
            .catch((err) => next(err));
        });
      } else {
        throw new Error("Unknown feedback type");
      }
    })
    .catch((err) => next(err));
});

router.put("/rejectfeedback", requiresLogin, (req, res, next) => {
  // Feedback rejection only changes the feedback status to rejected
  new Feedback({ id: req.body.id })
    .fetch()
    .then((feedback) => {
      feedback
        .save({ feedbackstatus_id: 2, reviewer_id: req.session.userID }, { patch: true })
        .then(() => {
          res.json({ success: true });
        })
        .catch((err) => next(err));
    })
    .catch((err) => next(err));
});

router.put("/undofeedback", requiresLogin, (req, res, next) => {
  new Feedback({ id: req.body.id })
    .fetch()
    .then((feedback) => {
      if (
        feedback.get("feedbackstatus_id") === 2 ||
        feedback.get("feedbackstatus_id") === 1 ||
        feedback.get("feedbacktype_id") === 1
      ) {
        // Undoing feedback rejection or comment feedbacks just changes the feedback status to new
        // Also undoing new feedbacks doesn't do anything (put request)
        feedback
          .save({ feedbackstatus_id: 1, reviewer_id: req.session.userID }, { patch: true })
          .then(() => {
            res.json({ success: true });
          })
          .catch((err) => next(err));
      } else if (feedback.get("feedbackstatus_id") === 3) {
        // Undoing approved feedback depends on the feedback type
        if (feedback.get("feedbacktype_id") === 2) {
          // Undoing new keyword changes enabled to false
          let search = {};
          if (feedback.get("keyword_id")) {
            search.id = feedback.get("keyword_id");
          } else {
            search.topic_id = feedback.get("topic_id");
            search.name = feedback.get("newname");
          }
          new Keyword(search)
            .fetch()
            .then((keyword) => {
              keyword
                .save({ enabled: false }, { patch: true })
                .then(() => {
                  feedback
                    .save({ feedbackstatus_id: 1, reviewer_id: req.session.userID }, { patch: true })
                    .then(() => {
                      res.json({ success: true });
                    })
                    .catch((err) => next(err));
                })
                .catch((err) => next(err));
            })
            .catch((err) => next(err));
        } else if (feedback.get("feedbacktype_id") === 3) {
          // Undoing keyword deletion finds it and changes enabled to true
          new Keyword({ id: feedback.get("keyword_id") }).fetch().then((keyword) => {
            keyword
              .save({ enabled: true }, { patch: true })
              .then(() => {
                feedback
                  .save({ feedbackstatus_id: 1, reviewer_id: req.session.userID }, { patch: true })
                  .then(() => {
                    res.json({ success: true });
                  })
                  .catch((err) => next(err));
              })
              .catch((err) => next(err));
          });
        } else if (feedback.get("feedbacktype_id") === 4) {
          // Undoing keyword modification changes the keyword name back to the old name
          new Keyword({ id: feedback.get("keyword_id") }).fetch().then((keyword) => {
            keyword
              .save({ name: feedback.get("oldname") }, { patch: true })
              .then(() => {
                feedback
                  .save({ feedbackstatus_id: 1, reviewer_id: req.session.userID }, { patch: true })
                  .then(() => {
                    res.json({ success: true });
                  })
                  .catch((err) => next(err));
              })
              .catch((err) => next(err));
          });
        } else {
          throw new Error("Unknown feedback type");
        }
      } else {
        // Invalid feedbacks can't be undone
        throw new Error("Cannot undo feedback that isn't accepted or rejected");
      }
    })
    .catch((err) => next(err));
});

module.exports = router;
