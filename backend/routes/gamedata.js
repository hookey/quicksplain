const express = require("express");
const router = express.Router();
const createError = require("http-errors");

const redisClient = require("../utils/redis-client");
const requiresLogin = require("../utils/admin-utils");
const { query } = require("express");

router.get("/getgamestates", requiresLogin, (req, res, next) => {
  redisClient.hgetall("quicksplain:gamestates", (err, data) => {
    if (err) next(err);
    if (!data) data = {};
    for (let key of Object.keys(data)) {
      data[key] = JSON.parse(data[key]);
    }
    res.json({ success: true, gameStates: data });
  });
});

module.exports = router;
