const express = require("express");
const router = express.Router();

const Category = require("../models/category");
const Topic = require("../models/topic");
const Keyword = require("../models/keyword");
const User = require("../models/user");
const Feedback = require("../models/feedback");
const FeedbackType = require("../models/feedbacktype");
const FeedbackStatus = require("../models/feedbackstatus");
const Turn = require("../models/turn");
const bookshelf = require("../utils/bookshelf");
const requiresLogin = require("../utils/admin-utils");
const { query } = require("express");

router.get("/getcategories", requiresLogin, (req, res, next) => {
  new Category()
    .query((qb) => {
      qb.orderBy("id");
    })
    .fetchAll()
    .then((categories) => {
      res.json(categories);
    })
    .catch((err) => next(err));
});

router.get("/getcategorywithtopics", requiresLogin, (req, res, next) => {
  new Category({ id: req.query.id })
    .fetch({
      withRelated: ["topics"],
      require: true,
    })
    .then((category) => {
      res.json(category);
    })
    .catch((err) => next(err));
});

router.get("/gettopicwithkeywords", requiresLogin, (req, res, next) => {
  new Topic({ id: req.query.id })
    .fetch({
      withRelated: ["keywords"],
      require: true,
    })
    .then((topic) => {
      res.json(topic);
    })
    .catch((err) => next(err));
});

router.put("/updatecategory", requiresLogin, (req, res, next) => {
  new Category({ id: req.body.category.id })
    .fetch()
    .then((category) => {
      category
        .save(req.body.category)
        .then(() => {
          res.json({ success: true });
        })
        .catch((err) => next(err));
    })
    .catch((err) => next(err));
});

router.put("/updatetopic", requiresLogin, (req, res, next) => {
  new Topic({ id: req.body.topic.id })
    .fetch()
    .then((topic) => {
      const oldTopic = topic.toJSON();
      if (oldTopic.name === req.body.topic.name && oldTopic.enabled === req.body.topic.enabled) {
        res.json({ success: true });
      } else {
        topic
          .save(req.body.topic)
          .then((topic) => {
            const newTopic = topic.toJSON();
            let comment = `Topic "${oldTopic.name}": `;
            let actions = [];
            if (newTopic.name !== oldTopic.name) {
              actions.push(`rename to "${newTopic.name}"`);
            }
            if (newTopic.enabled !== oldTopic.enabled) {
              actions.push(newTopic.enabled ? "enable" : "disable");
            }
            comment += actions.join(" and ");
            new Feedback({
              feedbacktype_id: 1,
              feedbackstatus_id: 3,
              author_id: req.session.userID,
              reviewer_id: req.session.userID,
              topic_id: newTopic.id,
              comment: comment,
              timestamp: new Date(),
            })
              .save()
              .then(() => {
                res.json({ success: true });
              })
              .catch((err) => next(err));
          })
          .catch((err) => next(err));
      }
    })
    .catch((err) => next(err));
});

router.put("/updatekeyword", requiresLogin, (req, res, next) => {
  new Keyword({ id: req.body.keyword.id })
    .fetch()
    .then((keyword) => {
      const oldKeyword = keyword.toJSON();
      if (oldKeyword.name === req.body.keyword.name && oldKeyword.enabled === req.body.keyword.enabled) {
        // If nothing was changed, don't return anything and show an error
        throw new Error("New keyword is same as old keyword.");
      } else {
        if (oldKeyword.name !== req.body.keyword.name) {
          // If name was changed, look for an accepted feedback with the opposite change and reject it
          // If that feedback doesn't exist, look for a new or rejected feedback with the same change and accept it
          // If that feedback doesn't exist either, make a new one
          new Feedback({
            feedbacktype_id: 4,
            feedbackstatus_id: 3,
            oldname: req.body.keyword.name,
            newname: oldKeyword.name,
          })
            .fetch({ require: false })
            .then((feedback) => {
              if (feedback) {
                console.log(feedback.toJSON());
                feedback
                  .save({ feedbackstatus_id: 2, reviewer_id: req.session.userID })
                  .catch((err) => console.error(err));
              } else {
                new Feedback({
                  feedbacktype_id: 4,
                  oldname: oldKeyword.name,
                  newname: req.body.keyword.name,
                })
                  .fetch({ require: false })
                  .then((feedback) => {
                    if (feedback) {
                      console.log(feedback.toJSON());
                      feedback
                        .save({ feedbackstatus_id: 3, reviewer_id: req.session.userID })
                        .catch((err) => console.error(err));
                    } else {
                      new Feedback({
                        feedbacktype_id: 4,
                        feedbackstatus_id: 3,
                        author_id: req.session.userID,
                        reviewer_id: req.session.userID,
                        topic_id: oldKeyword.topic_id,
                        keyword_id: oldKeyword.id,
                        comment: "Manually modified",
                        oldname: oldKeyword.name,
                        newname: req.body.keyword.name,
                        timestamp: new Date(),
                      })
                        .save()
                        .catch((err) => console.error(err));
                    }
                  })
                  .catch((err) => console.error(err));
              }
            })
            .catch((err) => console.error(err));
        }
        if (oldKeyword.enabled && !req.body.keyword.enabled) {
          // If keyword was disabled, check for a feedback that created it and reject it
          // If that doesn't exist, check for a feedback that wants to remove it and accept it
          // If that doesn't exist, make a new keyword removal feedback
          new Feedback({
            feedbacktype_id: 2,
            feedbackstatus_id: 3,
            keyword_id: oldKeyword.id,
          })
            .fetch({ require: false })
            .then((feedback) => {
              if (feedback) {
                feedback
                  .save({ feedbackstatus_id: 2, reviewer_id: req.session.userID })
                  .catch((err) => console.error(err));
              } else {
                new Feedback({
                  feedbacktype_id: 3,
                  keyword_id: oldKeyword.id,
                })
                  .fetch({ require: false })
                  .then((feedback) => {
                    if (feedback) {
                      feedback
                        .save({ feedbackstatus_id: 3, reviewer_id: req.session.userID })
                        .catch((err) => console.error(err));
                    } else {
                      new Feedback({
                        feedbacktype_id: 3,
                        feedbackstatus_id: 3,
                        author_id: req.session.userID,
                        reviewer_id: req.session.userID,
                        topic_id: oldKeyword.topic_id,
                        keyword_id: oldKeyword.id,
                        comment: "Manually removed",
                        oldname: req.body.keyword.name,
                        timestamp: new Date(),
                      })
                        .save()
                        .catch((err) => console.error(err));
                    }
                  });
              }
            })
            .catch((err) => console.error(err));
        } else if (!oldKeyword.enabled && req.body.keyword.enabled) {
          // If keyword was enabled, check for a feedback that removed it and reject it
          // If that doesn't exist, check for a feedback that wants to add it and accept it
          // If that doesn't exist (should not be possible), make a new keyword addition feedback
          new Feedback({
            feedbacktype_id: 3,
            feedbackstatus_id: 3,
            keyword_id: oldKeyword.id,
          })
            .fetch({ require: false })
            .then((feedback) => {
              if (feedback) {
                feedback
                  .save({ feedbackstatus_id: 2, reviewer_id: req.session.userID })
                  .catch((err) => console.error(err));
              } else {
                new Feedback({
                  feedbacktype_id: 2,
                  keyword_id: oldKeyword.id,
                })
                  .fetch({ require: false })
                  .then((feedback) => {
                    if (feedback) {
                      feedback
                        .save({ feedbackstatus_id: 3, reviewer_id: req.session.userID })
                        .catch((err) => console.error(err));
                    } else {
                      new Feedback({
                        feedbacktype_id: 2,
                        feedbackstatus_id: 3,
                        author_id: req.session.userID,
                        reviewer_id: req.session.userID,
                        topic_id: oldKeyword.topic_id,
                        keyword_id: oldKeyword.id,
                        comment: "Manually added",
                        newname: req.body.keyword.name,
                        timestamp: new Date(),
                      })
                        .save()
                        .catch((err) => console.error(err));
                    }
                  });
              }
            })
            .catch((err) => console.error(err));
        }
        // Either way, save modified keyword
        keyword
          .save(req.body.keyword)
          .then(() => {
            res.json({ success: true });
          })
          .catch((err) => next(err));
      }
    })
    .catch((err) => next(err));
});

router.put("/createcategory", requiresLogin, (req, res, next) => {
  new Category(req.body.category)
    .save()
    .then((category) => {
      res.json({ success: true, category: category.toJSON() });
    })
    .catch((err) => next(err));
});

router.put("/createtopic", requiresLogin, (req, res, next) => {
  new Topic(req.body.topic)
    .save()
    .then((topic) => {
      topic = topic.toJSON();
      new Feedback({
        feedbacktype_id: 1,
        feedbackstatus_id: 3,
        author_id: req.session.userID,
        reviewer_id: req.session.userID,
        topic_id: topic.id,
        comment: `Manually added topic "${topic.name}"`,
        timestamp: new Date(),
      })
        .save()
        .then(() => {
          res.json({ success: true, topic: topic });
        })
        .catch((err) => next(err));
    })
    .catch((err) => next(err));
});

router.put("/createkeyword", requiresLogin, (req, res, next) => {
  new Keyword(req.body.keyword)
    .save()
    .then((keyword) => {
      keyword = keyword.toJSON();
      new Feedback({
        feedbacktype_id: 2,
        feedbackstatus_id: 3,
        author_id: req.session.userID,
        reviewer_id: req.session.userID,
        topic_id: keyword.topic_id,
        keyword_id: keyword.id,
        comment: "Manually added",
        newname: keyword.name,
        timestamp: new Date(),
      })
        .save()
        .then(() => {
          res.json({ success: true, keyword: keyword });
        })
        .catch((err) => next(err));
    })
    .catch((err) => next(err));
});

module.exports = router;
