require("dotenv").config();

const express = require("express");
const app = express();

const indexRouter = require("./routes/index");
const feedbackRouter = require("./routes/feedback");
const keywordsRouter = require("./routes/keywords");
const gamedataRouter = require("./routes/gamedata");
const importexportRouter = require("./routes/importexport");
const statsRouter = require("./routes/stats");
const createError = require("http-errors");
const path = require("path");
const logger = require("morgan");

app.use(
  require("cors")({
    origin: process.env.ADMINPANEL_URL,
    credentials: true,
  })
);

const session = require("express-session");
const KnexSessionStore = require("connect-session-knex")(session);
const cookieParser = require("cookie-parser");

const knex = require("./utils/knex");
app.use(cookieParser());
app.use(
  session({
    secret: "secret3",
    resave: true,
    saveUninitialized: true,
    cookie: {
      maxAge: 24 * 60 * 60 * 1000, // 24 hours
    },
    store: new KnexSessionStore({
      knex,
      createtable: true,
    }),
  })
);

if (app.get("env") === "production") {
  app.set("trust proxy", 1); // trust first proxy
  sess.cookie.secure = true; // serve secure cookies
}

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/api/", indexRouter);
app.use("/api/feedback/", feedbackRouter);
app.use("/api/keywords/", keywordsRouter);
app.use("/api/gamedata/", gamedataRouter);
app.use("/api/importexport/", importexportRouter);
app.use("/api/stats/", statsRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};
  console.error(err);

  // render the error page
  res.status(err.status || 500);
  res.json({ success: false, message: err.message });
});

module.exports = app;
